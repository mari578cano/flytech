using Flytech.Data;
using Flytech.Helpers;
using Flytech.IRepository;
using Flytech.IService;
using Flytech.Repository;
using Flytech.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Flytech.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddAplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<ITokenExternalService, TokenExternalService>();
            services.AddScoped<ICollectionService, CollectionService>();
            services.AddScoped<ITokenExternalRepository, TokenExternalRepository>();
            services.AddScoped<ICollectionRepository, CollectionRepository>(); 
            services.AddScoped<IReportCollectionsRepository, ReportCollectionRepository>(); 
            services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            });
            
            return services;
        }

    }
}