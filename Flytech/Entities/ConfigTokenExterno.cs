using System;

namespace Flytech.Entities
{
    public class ConfigTokenExterno
    {
        public int Id { get; set; }
        public string TokenExternal { get; set; }   
        public DateTime Expiration { get; set; }
    }
}