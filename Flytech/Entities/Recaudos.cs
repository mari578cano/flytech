using System;

namespace Flytech.Entities
{
    public class Recaudos
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Station { get; set; }  
        public string Sense { get; set; }
        public string Hour { get; set; }
        public string Category { get; set; }
        public int Amount { get; set; }
        public double TabulatedValue { get; set; }
    }
}