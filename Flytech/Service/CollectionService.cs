using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Flurl.Http;
using Flytech.DTOs;
using Flytech.Entities;
using Flytech.Helpers;
using Flytech.IService;
using Microsoft.Extensions.Configuration;

namespace Flytech.Service
{
    public class CollectionService : ICollectionService
    {
        private readonly string _urlExternalFlyTech;
        private readonly ITokenExternalService _tokenExternal;
        
        public CollectionService(IConfiguration configuration, ITokenExternalService tokenExternal)
        {
            _urlExternalFlyTech = configuration["UrlExternalFlytech"];
            _tokenExternal = tokenExternal;
        }
        
         public async Task<List<ConteoDto>> GetAmountCars(string date)
        {
             var policy = HttpClient.BuildRetryPolicy();
             var token = await _tokenExternal.ExistToken();
             return await policy.ExecuteAsync(() => $"{_urlExternalFlyTech}/ConteoVehiculos/{date}" 
                 .WithOAuthBearerToken(token.TokenExternal)
                 .GetJsonAsync<List<ConteoDto>>()
             );
        }
        
         public async Task<List<RecaudoDto>> GetTabulatedValueCars(string date)
         {
             var policy = HttpClient.BuildRetryPolicy();
             var token = await _tokenExternal.ExistToken();
             return await policy.ExecuteAsync(() => $"{_urlExternalFlyTech}/RecaudoVehiculos/{date}" 
                 .WithOAuthBearerToken(token.TokenExternal)
                 .GetJsonAsync<List<RecaudoDto>>()
             );
         }
        
    }
}