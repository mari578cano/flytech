using System;
using System.Threading.Tasks;
using Flurl.Http;
using Flytech.DTOs;
using Flytech.Entities;
using Flytech.Helpers;
using Flytech.IRepository;
using Flytech.IService;
using Microsoft.Extensions.Configuration;

namespace Flytech.Service
{
    public class TokenExternalService  : ITokenExternalService
    {
        private readonly string _urlExternalFlyTech;
        private readonly ITokenExternalRepository _tokenExternalRepository;
        
        public TokenExternalService(IConfiguration configuration, ITokenExternalRepository tokenExternalRepository)
        {
            _urlExternalFlyTech = configuration["UrlExternalFlytech"];
            _tokenExternalRepository = tokenExternalRepository;
        }

        public async Task<ConfigTokenExterno> ExistToken()
        {
            var token = _tokenExternalRepository.GetToken();
            TokenExternoDto newToken;
            ConfigTokenExterno objToken;
            if (token == null)
            {
                newToken = GetTokenFlytech("user", "1234").GetAwaiter().GetResult();
                objToken = new ConfigTokenExterno
                {
                    Expiration = newToken.Expiration,
                    TokenExternal = newToken.Token
                };
                return await _tokenExternalRepository.SaveTokenExternal(objToken);
            }
            var time = token?.Expiration.CompareTo(DateTime.Now);
            if (time < 0)
            {
                newToken = GetTokenFlytech("user", "1234").GetAwaiter().GetResult();
                objToken = new ConfigTokenExterno
                {
                    Expiration = newToken.Expiration,
                    TokenExternal = newToken.Token
                };
                
                return await _tokenExternalRepository.UpdateTokenExternal(objToken);
            }

            return _tokenExternalRepository.GetToken();
        }

        public async Task<TokenExternoDto> GetTokenFlytech(string UserName, string Password)
        {
            var policy = HttpClient.BuildRetryPolicy();

            return await policy.ExecuteAsync(() => $"{_urlExternalFlyTech}/login"
                .PostJsonAsync(new
                {
                    UserName = UserName,
                    Password = Password,
                })
                .ReceiveJson<TokenExternoDto>()
            );
        }
        
    }
}