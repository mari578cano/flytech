using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Flytech.Data;
using Flytech.DTOs;
using Flytech.Entities;
using Flytech.IRepository;
using Microsoft.EntityFrameworkCore;

namespace Flytech.Repository
{
    public class TokenExternalRepository : ITokenExternalRepository
    {
        private readonly DataContext _context;
        
        public TokenExternalRepository(DataContext context)
        {
            _context = context;
        }
        public ConfigTokenExterno GetToken()
        {
            return _context.ConfigTokenExternals.ToList().FirstOrDefault();
        }

        public async Task<ConfigTokenExterno> SaveTokenExternal(ConfigTokenExterno configTokenExterno)
        {
            _context.ConfigTokenExternals.Add(configTokenExterno);
            await _context.SaveChangesAsync();

            return configTokenExterno;
        }

        public async Task<ConfigTokenExterno> UpdateTokenExternal(ConfigTokenExterno configTokenExterno)
        {
            var entity = GetToken();
            if (entity != null)
            {
                entity.TokenExternal = configTokenExterno.TokenExternal;
                entity.Expiration = configTokenExterno.Expiration;
                _context.ConfigTokenExternals.Update(entity);
                await _context.SaveChangesAsync();

                return entity;
            }
            else
            {
                return null;
            }
        }
    }
}