using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Flytech.Data;
using Flytech.DTOs;
using Flytech.DTOs.Reporting;
using Flytech.Entities;
using Flytech.IRepository;

namespace Flytech.Repository
{
    public class CollectionRepository : ICollectionRepository
    {
        private DataContext _context;
        private readonly IMapper _mapper;
        public CollectionRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        
        public async Task<bool> SaveDataConteo(List<ConteoDto> list, string date)
        {
            var entity = _mapper.Map<List<Recaudos>>(list);
            var dateConvert = Convert.ToDateTime(date);
            foreach (var item in entity)
            {
                item.DateCreated = dateConvert;
            }
            await _context.Recaudos.AddRangeAsync(entity);
            await _context.SaveChangesAsync();

            return true;
        }
        
        public async Task<bool> SaveDataRecaudo(List<RecaudoDto> list, string date)
        {
            var entity = _mapper.Map<List<Recaudos>>(list);
            var dateConvert = Convert.ToDateTime(date);
            foreach (var item in entity)
            {
                item.DateCreated = dateConvert;
            }
            await _context.Recaudos.AddRangeAsync(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public List<Recaudos> ListDataCollections(string station, string sense, string category)
        {
            // pending change filter with predicated
            if (station != null && sense != null && category != null)
            {
                return _context.Recaudos.Where(r => r.Station == station
                                                    && r.Sense == sense && r.Category == category).ToList();
            }
            else if (station != null)
            {
                return _context.Recaudos.Where(r => r.Station == station).ToList();
            }
            else if (sense != null)
            {
                return _context.Recaudos.Where(r => r.Sense == sense).ToList();
            }
            else if (category != null)
            {
                return _context.Recaudos.Where(r => r.Category == category).ToList();
            }

            return _context.Recaudos.ToList();

        }

        public List<string> ListStations()
        {
            var result = _context.Recaudos.Select(x =>   x.Station).ToList();
            
            return result.Distinct().ToList();
        }
    }
}