using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flytech.Data;
using Flytech.DTOs.Reporting;
using Flytech.Entities;
using Flytech.IRepository;

namespace Flytech.Repository
{
    public class ReportCollectionRepository : IReportCollectionsRepository
    {
        private readonly DataContext _context;
        private readonly ICollectionRepository _collection;

        public ReportCollectionRepository(DataContext context,
            ICollectionRepository collection)
        {
            _context = context; 
            _collection = collection;
        }

        public List<DateTime> GetDates()
        {
            var result = _context.Recaudos.Select(x => x.DateCreated).ToList();
            return result.Distinct().ToList();
        }

        public List<TotalValuesStationsDto> GetDataValuesStations()
        {
            var resultGetDataJoinStationDate = new List<TotalValuesStationsDto>();
            var resultDates = GetDates();

            foreach (var date in resultDates)
            {
                // I get the results filtered by date
                var resultDataCollections = GetDataForDate(date);
                // I send this result to GetDataJoinStationDate which is already filtered by date
                resultGetDataJoinStationDate.AddRange(GetDataJoinStationDate(resultDataCollections, date));
            }

            return resultGetDataJoinStationDate;
        }
        
        private List<Recaudos> GetDataForDate(DateTime date)
        {
            // Get all data pertaining to that sent date
            var result = _context.Recaudos.Select(x =>  new Recaudos
            {
                Id = x.Id,
                Station = x.Station,
                DateCreated = x.DateCreated,
                Amount = x.Amount,
                TabulatedValue = x.TabulatedValue
            }).Where(x => x.DateCreated == date).ToList();

            return result;
        }

        private IEnumerable<TotalValuesStationsDto> GetDataJoinStationDate(IReadOnlyCollection<Recaudos> listCollection, DateTime dateFilter)
        {
            var listTotalValues = new List<TotalValuesStationsDto>();
            // Get the station
            var stations = _collection.ListStations();
            
            // Go through the list of stations and filter on the list of collections that I enter
            foreach (var station in stations)
            {
                var objTotalValuesStation = new TotalValuesStationsDto();
                var result = listCollection.Where(x => x.Station == station).ToList();
                // The data is sent fully filtered by the sum of the quantity and the value
                var (amount, total) = SumCollection(result);
                objTotalValuesStation.Station = station;
                objTotalValuesStation.DateReport = dateFilter;
                objTotalValuesStation.TotalAmount = amount;
                objTotalValuesStation.Total = total;

                listTotalValues.Add(objTotalValuesStation);
            }

            return listTotalValues;
        }

        // Sum values collection for station
        private static (int amount, double total) SumCollection(List<Recaudos> listCollection)
        {
            var sumAmount = 0;
            double sumTotal = 0;
            foreach (var collection in listCollection)
            {
                sumAmount = sumAmount + collection.Amount;
                sumTotal = sumTotal + collection.TabulatedValue;
            }

            return (sumAmount, sumTotal);
        }

        public ReportValuesGeneralDto GetDataGeneral(List<TotalValuesStationsDto> totalValuesStations)
        {
            // Get the station
            var stations = _collection.ListStations();
            var objTotalValuesStationGeneral = new ReportValuesGeneralDto();
            var listValuesGeneral = new List<ReportValuesGeneralStationDto>();
            objTotalValuesStationGeneral.Title = "Total";
            objTotalValuesStationGeneral.ValuesTotal = new List<ReportValuesGeneralStationDto>();
            
            // Go through the list of stations and filter on the list of collections that I enter
            foreach (var station in stations)
            {
                var objValuesGeneral = new ReportValuesGeneralStationDto();
                var result = totalValuesStations.Where(x => x.Station == station).ToList();
                // The data is sent fully filtered by the sum of the quantity and the value
                var (amount, total) = SumCollection(result);
                objValuesGeneral.TotalAmountGeneral = amount;
                objValuesGeneral.Station = station;
                objValuesGeneral.TotalGeneral = total;
                listValuesGeneral.Add(objValuesGeneral);
            }
            
            objTotalValuesStationGeneral.ValuesTotal.AddRange(listValuesGeneral);
            return objTotalValuesStationGeneral;
        }

        // Sum values general 
        private static (int amount, double total) SumCollection(List<TotalValuesStationsDto> listValuesForStation)
        {
            var sumAmount = 0;
            double sumTotal = 0;
            foreach (var collection in listValuesForStation)
            {
                sumAmount = sumAmount + collection.TotalAmount;
                sumTotal = sumTotal + collection.Total;
            }

            return (sumAmount, sumTotal);
        }
        
        // Sum values global 
        private static (int amount, double total) SumCollection(List<ReportValuesGeneralStationDto> listValuesGeneral)
        {
            var sumAmount = 0;
            double sumTotal = 0;
            foreach (var values in listValuesGeneral)
            {
                sumAmount = sumAmount + values.TotalAmountGeneral;
                sumTotal = sumTotal + values.TotalGeneral;
            }

            return (sumAmount, sumTotal);
        }
        
        public TotalGlobalDto GetDataGlobal(ReportValuesGeneralDto totalValuesStations)
        {

            var objTotalValuesGlobal = new TotalGlobalDto();
            // Sum values general
            var (amount, total) = SumCollection(totalValuesStations.ValuesTotal);
            objTotalValuesGlobal.Title = "Global";
            objTotalValuesGlobal.AmountGlobal = amount;
            objTotalValuesGlobal.TotalGlobal = total;
            
            return objTotalValuesGlobal;
        }

        private List<SetReportDatesStationsValuesDto> ConvertReportValuesStation(List<TotalValuesStationsDto> list)
        {
            var listResult = new List<SetReportDatesStationsValuesDto>();
            var groupDates = list.GroupBy(x => x.DateReport);
            foreach (var groupDate in groupDates)
            {
                var objSetReportDatesStationsValuesDto = new SetReportDatesStationsValuesDto
                {
                    ListValues = new List<ReportValuesDto>()
                };
                var groupKey = groupDate.Key;
                objSetReportDatesStationsValuesDto.DateCreated = groupKey;
                foreach (var item in groupDate)
                {
                    var objItemValues = new ReportValuesDto();
                    objItemValues.Total = item.Total;
                    objItemValues.TotalAmount = item.TotalAmount;
                    objSetReportDatesStationsValuesDto.ListValues.Add(objItemValues);
                }
                listResult.Add(objSetReportDatesStationsValuesDto);
            }

            return listResult;
        }

        public ReportCollectionsTabulatedDto ReportTabulated()
        {
            var objReportTabulated = new ReportCollectionsTabulatedDto();
            var listStations = _collection.ListStations();
            var listTotalStationsDate = GetDataValuesStations();
            var listTotalStationsGeneral = GetDataGeneral(listTotalStationsDate);
            var objTotalGlobalDto = GetDataGlobal(listTotalStationsGeneral);
            var groupDateStation = ConvertReportValuesStation(listTotalStationsDate);

            objReportTabulated.Stations = listStations;
            objReportTabulated.TotalStationsDate = groupDateStation;
            objReportTabulated.TotalStationsGeneral = listTotalStationsGeneral;
            objReportTabulated.TotalGlobal = objTotalGlobalDto;

            return objReportTabulated;
        }
    }
}