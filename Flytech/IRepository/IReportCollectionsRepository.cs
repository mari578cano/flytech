using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Flytech.DTOs.Reporting;
using Flytech.Entities;

namespace Flytech.IRepository
{
    public interface IReportCollectionsRepository
    {
        List<DateTime> GetDates();
        List<TotalValuesStationsDto> GetDataValuesStations();
        ReportValuesGeneralDto GetDataGeneral(List<TotalValuesStationsDto> totalValuesStations);
        TotalGlobalDto GetDataGlobal(ReportValuesGeneralDto totalValuesStations);

        ReportCollectionsTabulatedDto ReportTabulated();
    }
}