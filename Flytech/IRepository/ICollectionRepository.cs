using System.Collections.Generic;
using System.Threading.Tasks;
using Flytech.DTOs;
using Flytech.DTOs.Reporting;
using Flytech.Entities;

namespace Flytech.IRepository
{
    public interface ICollectionRepository
    {
        Task<bool> SaveDataConteo(List<ConteoDto> list, string date);
        Task<bool> SaveDataRecaudo(List<RecaudoDto> list, string date);

        List<Recaudos> ListDataCollections(string station, string sense, string category);

        List<string> ListStations();
    }
}