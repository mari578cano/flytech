using System.Threading.Tasks;
using Flytech.DTOs;
using Flytech.Entities;

namespace Flytech.IRepository
{
    public interface ITokenExternalRepository
    {
        ConfigTokenExterno GetToken();
        Task<ConfigTokenExterno> SaveTokenExternal(ConfigTokenExterno configTokenExterno);
        Task<ConfigTokenExterno> UpdateTokenExternal(ConfigTokenExterno configTokenExterno);
    }
}