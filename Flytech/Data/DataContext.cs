using Flytech.Entities;
using Microsoft.EntityFrameworkCore;

namespace Flytech.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<Recaudos> Recaudos { get; set; }
        
        public DbSet<Usuarios> Usuarios { get; set; }
        
        public DbSet<ConfigTokenExterno> ConfigTokenExternals { get; set; }
    }
}