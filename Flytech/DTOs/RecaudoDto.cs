namespace Flytech.DTOs
{
    public class RecaudoDto
    {
        public string Estacion { get; set; }  
        public string Sentido { get; set; }
        public string Hora { get; set; }
        public string Categoria { get; set; }
        public string ValorTabulado { get; set; }
    }
}