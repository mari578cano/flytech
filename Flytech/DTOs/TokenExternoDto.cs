using System;

namespace Flytech.DTOs
{
    public class TokenExternoDto
    {
        public string Token { get; set; }   
        public DateTime Expiration { get; set; }
    }
}