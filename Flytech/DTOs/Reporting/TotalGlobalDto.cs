namespace Flytech.DTOs.Reporting
{
    public class TotalGlobalDto
    {
        public string Title { get; set; }
        public int AmountGlobal { get; set; }
        public double TotalGlobal { get; set; }
    }
}