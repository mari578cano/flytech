using System.Collections.Generic;
using System.ComponentModel;

namespace Flytech.DTOs.Reporting
{
    public class StationsDto
    {
        public string Station { get; set; }
    }
}