namespace Flytech.DTOs.Reporting
{
    public class ReportValuesDto
    {
        public string Station { get; set; }
        public int TotalAmount { get; set; }
        public double Total { get; set; }
    }
}