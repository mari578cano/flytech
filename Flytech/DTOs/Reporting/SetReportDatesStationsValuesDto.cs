using System;
using System.Collections.Generic;

namespace Flytech.DTOs.Reporting
{
    public class SetReportDatesStationsValuesDto
    {
        public DateTime DateCreated { get; set; }
        public List<ReportValuesDto> ListValues { get; set; }
    }
}