using System;
using System.Collections.Generic;

namespace Flytech.DTOs.Reporting
{
    public class TotalValuesStationsDto
    {
        public DateTime DateReport { get; set; }
        public string Station { get; set; }
        public int TotalAmount { get; set; }
        public double Total { get; set; }
    }
}