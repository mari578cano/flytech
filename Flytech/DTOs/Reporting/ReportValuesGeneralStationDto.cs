namespace Flytech.DTOs.Reporting
{
    public class ReportValuesGeneralStationDto
    {
        public string Station { get; set; }
        public int TotalAmountGeneral { get; set; }
        public double TotalGeneral { get; set; }
    }
}