using System.Collections.Generic;

namespace Flytech.DTOs.Reporting
{
    public class ReportCollectionsTabulatedDto
    {
        public List<string> Stations { get; set; }
        public List<SetReportDatesStationsValuesDto>  TotalStationsDate { get; set; }
        public ReportValuesGeneralDto TotalStationsGeneral { get; set; }
        public TotalGlobalDto TotalGlobal { get; set; }
        
    }
}