using System.Collections.Generic;
using System.Dynamic;

namespace Flytech.DTOs.Reporting
{
    public class ReportValuesGeneralDto
    {
        public string Title { get; set; }
        public List<ReportValuesGeneralStationDto> ValuesTotal { get; set; }
        
    }
}