namespace Flytech.DTOs
{
    public class FilterDto
    {
        public string Station { get; set; }
        public string Sense { get; set; }
        public string Category { get; set; }
    }
}