using System.Threading.Tasks;
using Flytech.DTOs;
using Flytech.Entities;

namespace Flytech.IService
{
    public interface ITokenExternalService
    {
        Task<TokenExternoDto> GetTokenFlytech(string UserName, string Password);
        Task<ConfigTokenExterno> ExistToken();
    }
}