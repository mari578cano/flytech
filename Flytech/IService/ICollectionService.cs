using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Flytech.DTOs;
using Flytech.Entities;

namespace Flytech.IService
{
    public interface ICollectionService
    {
        Task<List<ConteoDto>> GetAmountCars(string date);
        Task<List<RecaudoDto>> GetTabulatedValueCars(string date);
    }
}