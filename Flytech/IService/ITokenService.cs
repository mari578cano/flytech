using Flytech.Entities;

namespace Flytech.IService
{
    public interface ITokenService
    {
        string CreateToken(Usuarios user);
    }
}