using AutoMapper;
using Flytech.DTOs;
using Flytech.Entities;

namespace Flytech.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            // Entity to Dto Conteo
            CreateMap<Recaudos, ConteoDto>()
                .ForMember(dest => dest.Estacion,
                    opt => opt.MapFrom(src => src.Station))
                .ForMember(dest => dest.Sentido,
                    opt => opt.MapFrom(src => src.Sense))
                .ForMember(dest => dest.Hora,
                    opt => opt.MapFrom(src => src.Hour))
                .ForMember(dest => dest.Categoria,
                    opt => opt.MapFrom(src => src.Category))
                .ForMember(dest => dest.Cantidad,
                    opt => opt.MapFrom(src => src.Amount));
            
            // Dto to Entity Conteo
            CreateMap<ConteoDto, Recaudos>()
                .ForMember(dest => dest.Station,
                    opt => opt.MapFrom(src => src.Estacion))
                .ForMember(dest => dest.Sense,
                    opt => opt.MapFrom(src => src.Sentido))
                .ForMember(dest => dest.Hour,
                    opt => opt.MapFrom(src => src.Hora))
                .ForMember(dest => dest.Category,
                    opt => opt.MapFrom(src => src.Categoria))
                .ForMember(dest => dest.Amount,
                    opt => opt.MapFrom(src => src.Cantidad));
            
            // Entity to Dto Recaudo
            CreateMap<Recaudos, RecaudoDto>()
                .ForMember(dest => dest.Estacion,
                    opt => opt.MapFrom(src => src.Station))
                .ForMember(dest => dest.Sentido,
                    opt => opt.MapFrom(src => src.Sense))
                .ForMember(dest => dest.Hora,
                    opt => opt.MapFrom(src => src.Hour))
                .ForMember(dest => dest.Categoria,
                    opt => opt.MapFrom(src => src.Category))
                .ForMember(dest => dest.ValorTabulado,
                    opt => opt.MapFrom(src => src.TabulatedValue));
            
            // Dto to Entity Recaudo
            CreateMap<RecaudoDto, Recaudos>()
                .ForMember(dest => dest.Station,
                    opt => opt.MapFrom(src => src.Estacion))
                .ForMember(dest => dest.Sense,
                    opt => opt.MapFrom(src => src.Sentido))
                .ForMember(dest => dest.Hour,
                    opt => opt.MapFrom(src => src.Hora))
                .ForMember(dest => dest.Category,
                    opt => opt.MapFrom(src => src.Categoria))
                .ForMember(dest => dest.TabulatedValue,
                    opt => opt.MapFrom(src => src.ValorTabulado));
        }
    }
}