using System;
using System.Linq;
using System.Net;
using Flurl.Http;
using Polly;
using Polly.Retry;

namespace Flytech.Helpers
{
    public static class HttpClient
    {
        private static bool IsTrasientError(FlurlHttpException exception)
        {
            int[] httpStatusCodesWorthRetrying =
            {
                (int)HttpStatusCode.RequestTimeout, //408
                (int)HttpStatusCode.BadGateway, // 502
                (int)HttpStatusCode.ServiceUnavailable, // 503
                (int)HttpStatusCode.GatewayTimeout // 504
            };
            
            return exception.StatusCode.HasValue && httpStatusCodesWorthRetrying.Contains(exception.StatusCode.Value);
        }

        public static AsyncRetryPolicy BuildRetryPolicy()
        {
            var retryPolice = Policy
                .Handle<FlurlHttpException>(IsTrasientError)
                .WaitAndRetryAsync(3, retryAttempt =>
                {
                    var nextAttemptIn = TimeSpan.FromSeconds(Math.Pow(2, retryAttempt));
                    Console.WriteLine($"Reintentar intento {retryAttempt} para hacer la solicitud. Proximo intento en {nextAttemptIn.TotalSeconds} segundos");
                    return nextAttemptIn;
                });
            
            return retryPolice;
        }
    }
}