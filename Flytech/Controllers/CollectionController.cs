using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Flytech.Data;
using Flytech.Entities;
using Flytech.IRepository;
using Flytech.IService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flytech.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class Collection : Controller
    {
        private readonly DataContext _context;
        private readonly ICollectionRepository _collection;
        private readonly ICollectionService _collectionService;
        
        public Collection(DataContext context, ICollectionService collectionService,
            ICollectionRepository collection)
        {
            _context = context;
            _collectionService = collectionService;
            _collection = collection;
        }

        [HttpGet("AmountCarsExternal/{date}")]
        public async Task<ActionResult> GetAmountCarsExternal(string date)
        {
            var result = await _collectionService.GetAmountCars(date);
            if (result == null) return BadRequest(false);
            var resultSave = await _collection.SaveDataConteo(result, date);
                
            return resultSave == true ? Ok(true) : BadRequest(false);

        }
        
        [HttpGet("CollectionCarsExternal/{date}")]
        public async Task<ActionResult> GetCollectionCarsExternal(string date)
        {
            var result = await _collectionService.GetTabulatedValueCars(date);
            if (result == null) return BadRequest(false);
            var resultSave = await _collection.SaveDataRecaudo(result, date);

            return resultSave == true ? Ok(true) : BadRequest(false);
        }
        
        [HttpGet("CollectionFilters")]
        public ActionResult<List<Recaudos>> GetCollectionCarsExternal(string station, string sense, string category)
        {
            var result = _collection.ListDataCollections(station, sense, category);

            if (result != null)
                return Ok(result);
            else
                return BadRequest();
        }
    }
}