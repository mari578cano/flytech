using System.Threading.Tasks;
using Flytech.Data;
using Flytech.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flytech.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ReportCollectionController : Controller
    {
        private readonly DataContext _context;
        private readonly IReportCollectionsRepository _report;

        public ReportCollectionController(DataContext context, IReportCollectionsRepository report)
        {
            _context = context;
            _report = report;
        }
        
        [HttpGet("GetDataStations")]
        public ActionResult GetDataDataStations()
        {
            var result = _report.GetDataValuesStations();
            if (result != null)
                return Ok(result);
            else
                return BadRequest();
        }
        
        [HttpGet("GetDataTotalGeneral")]
        public ActionResult GetDataTotalGeneral()
        {
            var resultStations = _report.GetDataValuesStations();
            var resultTotalGeneral = _report.GetDataGeneral(resultStations);
            if (resultTotalGeneral != null)
                return Ok(resultTotalGeneral);
            else
                return BadRequest();
        }
        
        [HttpGet("GetDataTotalGlobal")]
        public ActionResult GetDataTotalGlobal()
        {
            var resultStations = _report.GetDataValuesStations();
            var resultTotalGeneral = _report.GetDataGeneral(resultStations);
            var resultGlobal = _report.GetDataGlobal(resultTotalGeneral);
            if (resultGlobal != null)
                return Ok(resultGlobal);
            else
                return BadRequest();
        }
        
        [HttpGet("GetReport")]
        public ActionResult GetReport()
        {
            var result = _report.ReportTabulated();
            if (result != null)
                return Ok(result);
            else
                return BadRequest();
        }
    }
}